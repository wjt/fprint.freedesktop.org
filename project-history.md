% lifprint — Project history
% Bastien Nocera, Daniel Drake
% 2018

# Project history

## The start: dpfp

In September 2005, Tony Vroon donated Daniel Drake a Microsoft fingerprint reader, with the task of trying to get it running on Linux.

After a slow start, I had an initial incarnation of the driver working and [could see my finger!](http://www.reactivated.net/weblog/archives/2006/01/fingerprinting/).

The Microsoft devices are actually rebranded DigitalPersona devices, but the DigitalPersona drivers do encrypt the image data. It turns out that the firmware controls encryption, and the [Microsoft-supplied firmware disables encryption](http://www.reactivated.net/weblog/archives/2006/01/breaking-encryption-the-easy-way/). So, we could then support the DigitalPersona devices by tweaking the firmware.

This was the [dpfp](http://dpfp.berlios.de/) project in early stages.

Mikko Kiviharju, a Finnish security expert, was researching the devices for a BlackHat Europe paper at the same time. Mikko combined our findings and [generated some press](http://www.itnews.com.au/newsstory.aspx?CIaNID=30692) with the results.

## Parallel work

There weren't many other people thinking about Linux fingerprinting at this time, but there were some projects. UPEK provided binary drivers against BioAPI for their hardware, which Pavel Machek reverse-engineered: it turns out they don't have the imaging problem because UPEK do fingerprint matching in hardware!

A couple of other projects existed which could get images from other devices, but could not do any more.

## Image processing and export control

We can download images from the Microsoft devices, but how do we login with them? We need some kind of image processing, and then comparison of the processed images. Not a small problem.

[I looked into some open source solutions](http://www.reactivated.net/weblog/archives/2006/08/fingerprint-enhancement-and-recognition/) but didn't get too far. In September 2006 I started playing with [NBIS](https://www.nist.gov/services-resources/software/nist-biometric-image-software-nbis) (formerly NFIS2), code from the US government to process and compare fingerprints. It [worked splendidly](http://www.reactivated.net/weblog/archives/2006/10/nfis2-works/), but the project halted due to concerns that we couldn't distribute this from the US without violating the U.S. Export Administration Regulations.

The [Software Freedom Law Center](http://www.softwarefreedom.org/) provided a little advice but this did not really go anywhere or produce any positive findings. NIST themselves were responsive by email but did not like to talk about the legal side, they weren't really sure about the export control restrictions on this software but were playing it safe by limiting distribution to CD only.

## The university project

After some inactivity due to the export concerns, things started happening. I had to choose a 3rd year university project (Computer Science at The University of Manchester), and I proposed a fingerprinting project of broader scale. The university accepted my proposal, agreed it could be an open source project, and the **fprint** idea was born! [Toby Howard](http://www.cs.man.ac.uk/~toby/) accepted the proposal and became the tutor for the project.

Preparing to leaving the US, I sat down and read the US Export Administration Regulations in detail, and discovered that in an open project we are free to redistribute this code after all. I confirmed this in a discussion with the exports office.

I started designing and developing fprint in October 2007. As much as I dislike open source code not being publicly available from the start, as this was an assessed project I felt it necessary to implement the fundamentals myself. The project was then opened up for outside contributions.

THE UNIVERSITY OF MANCHESTER DO NOT ENDORSE THIS THIS SOFTWARE AND ARE IN NO WAY RESPONSIBLE FOR THE CODE CONTAINED WITHIN, OR ANY DAMAGES CAUSED BY IT. Development does not happen on university computers and the project is not hosted at the university either.

After assessment, I [published the report](http://www.reactivated.net/fprint/academic-project/fprint_report.pdf).

## Open source project continuation

After university assessment, the project continues with Bastien Nocera becoming the primary maintainer, Vasily Khoruzhick stepping up to libfprint maintainer in 2016, after years as a developer, and contributions from a wide range of people being included. The project is now hosted by freedesktop.
